const express = require("express")
const app= express()
const { PORT = 8000 } = process.env;
const env = require("./env.json")
const cors = require('cors')
app.use(cors())
app.use(express.json())

const { QueryTypes } = require('sequelize');


//database
const db = require('./db')
db.authenticate()
.then(()=> console.log("db connected"))
.catch(err=> console.log('error ' + err))

app.get('/',(req, res)=> res.send('index'))

//Routes

//get user and encounters

app.post('/getuserinfo', async(req, res)=>{
    returObject={
        username: req.body.username,
        spotted: []
    }

    try{

        console.log("getuserinfo called")
        const userinfo = await db.query(`select pokeid, shiny, gender, longitude, latitude from usertable, encounter where username = spottedby and usertable.username like '%${returObject.username}%'`, {type: QueryTypes.SELECT});
            
            returObject.spotted=userinfo
            
            console.log(returObject)

        res.json(returObject)


    }catch(err){
        console.log(err)
    }
})

//get most spotted pokemons
app.get('/mostcommon', async(req, res)=>{
    console.log('mostcommon was called')
    try{

    const mostCommon = await db.query(`select count(pokeid) as timesbeenspotted, pokeid from encounter group by pokeid order by pokeid`, {type: QueryTypes.SELECT});
       
        res.json(mostCommon)

    }catch(err){

       
        console.log(err)
    }
})


//get top 10 users
app.get('/topspotters', async(req, res)=>{
    try{

    const topspotters = await db.query(`select count(spottedby) as numberofspotted, spottedby from encounter  group by spottedby order by numberofspotted desc limit 10`, {type: QueryTypes.SELECT});
        console.log(topspotters)
        res.json(topspotters)

    }catch(err){

       
        console.log(err)
    }
})

//Get userranking
app.post('/getranking', async(req, res)=>{
    try{
       let username=req.body.username
       
       console.log("USERNAME: "+ username)

    let ranking = await db.query(`select count(spottedby) as numberofspotted, spottedby,
    DENSE_RANK () OVER (ORDER BY count (spottedby) desc)
    from encounter  group by spottedby `, {type: QueryTypes.SELECT});
        
            console.log(ranking)
           let rank=ranking.find(obj=>obj.spottedby==username)
        //rank=ranking.find(rank=> rank.id=username)
        console.log(rank)
        res.json(rank)

    }catch(err){
        console.log(err)
    }
})


//Add spotted pokemon
app.post('/addencounter', async(req, res)=>{

    try {
        console.log("addencounter was called")
        const spottedby = req.body.spottedby
        let pokeid = req.body.pokeid
        let shiny = req.body.shiny
        let gender = req.body.gender
        let longitude = req.body.longitude
        let latitude = req.body.latitude
        
      
        db.query(`insert into encounter (spottedby, pokeid, shiny, gender, longitude, latitude) VALUES('${spottedby}', ${pokeid}, ${shiny}, ${gender}, ${longitude}, ${latitude} )`, {type: QueryTypes.INSERT} )
        .then(newEncounter => res.json(newEncounter)
        )

    } catch (error) {
        console.log(error)
    }
})


//create user

app.post('/createuser', async (req, res)=>{
        let email =''
        if(req.body.email){
            email=req.body.email
        }

    try{
        console.log("create user was called")
        let username = req.body.username
        let psw = req.body.password
        

        db.query(`insert into usertable (username, password, email) VALUES('${username}', '${psw}','${email}')`, {type: QueryTypes.INSERT} )
        .then(newUser => res.json(newUser)
        )
        

        }catch(err){
            console.log("err: " + err)
        }
}  
)

app.post('/verifyuser', async(req, res)=>{
    try{
        console.log("verify user")
        const username = req.body.username
        const psw = req.body.password

        db.query(`select username, email from usertable where usertable.username='${username}' and usertable.password='${psw}'`, {type: QueryTypes.SELECT} )
         .then(returnUser => res.json(returnUser)
         )
        

        }catch(err){
            console.log("err: " + err)
        }
}  
)




app.get('/getrarest', async(req, res)=>{
    try{
        db.query(`select count(pokeid) as timesbeenspotted, pokeid from encounter group by pokeid order by timesbeenspotted asc limit 10`, {type: QueryTypes.SELECT})
        .then(rarest=res.json(rarest))

    }catch(err){
        console.log(err)
    }
})

app.post('/getnotspotted', async(req, res)=>{
    let pokearray=[]
    for(i=0;i<151;i++){
        pokearray.push(i+1)
    }
    returObject={
        username: req.body.username,
        notspotted: []
    }
    try{
        console.log("getnotspotted called")
        const userinfo = await db.query(`select pokeid, shiny, gender, longitude, latitude from usertable, encounter where username = spottedby and usertable.username like '%${returObject.username}%'`, {type: QueryTypes.SELECT});
 
            for(i=0;i<returObject.spotted.length; i++){
               

               for(j=0;j<pokearray.length;j++){
                if(returObject.spotted[i].pokeid==pokearray[j]){
                    delete pokearray[j]
                }
               }
            }
         
               pokearray = pokearray.filter(n => n)

            returObject.spotted=pokearray

        res.json(returObject)


    }catch(err){
        console.log(err)
    }
    
})


app.listen(PORT, ()=> console.log(`server started on port ${PORT}`))




// select count(spottedby) as numberofspotted, spottedby,
// ROW_NUMBER () OVER (ORDER BY count (spottedby) desc)
// from encounter  group by spottedby 